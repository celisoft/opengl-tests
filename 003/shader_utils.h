#ifndef SHADER_UTIlS_H
#define SHADER_UTILS_H

#include <string>
#include <GL/glew.h>

//Load a file and return its content
const char* load(std::string pFilePath);

//Use to debug a shader (for the moment only shaders)
void debug(GLuint gl_object);

//Create the shader of the given type from a file
GLuint create_shader(std::string pFilePath, GLenum type);

#endif
