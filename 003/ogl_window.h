#ifndef OGL_WINDOW_H
#define OGL_WINDOW_H

#include <string>

//Must be loaded before SDL_opengl.h
#include <GL/glew.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

class OGLWindow
{
	private:
 		bool is_running;  

		std::string base_path;		

		SDL_Window* display;
		SDL_GLContext context;

		GLuint vbo_triangle;
		GLuint program;
		GLint attribute_coord2d, attribute_colors;

		float current_alpha;
		GLint uniform_alpha;

	public:
		//Constructor
		OGLWindow()
		{
			display = nullptr;
		}

		//Initialize the display
		bool init();

		//Create GLSL resources
		bool init_resources(void);

		//Display the game
		bool run();

		//Handle events
		void on_event(SDL_Event* event);
};

#endif
